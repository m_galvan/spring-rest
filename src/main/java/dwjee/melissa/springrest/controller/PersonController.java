package dwjee.melissa.springrest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dwjee.melissa.springrest.entity.Person;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;


@RestController
@RequestMapping("/api")
public class PersonController {

    @GetMapping("/person")
    public List<Person> getAll() {
        Person person1 = new Person(1, "Galvan", "Melissa", 27);
        Person person2 = new Person(2, "Bola", "Prescillia", 26);
        Person person3 = new Person(3, "Galvan", "Nathy", 23);
        List<Person> persons = new ArrayList<>(List.of(person1, person2, person3));
        return persons;
    }
}
