package dwjee.melissa.springrest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
@CrossOrigin("*")
public class AppController {
    public String index() {
        return "index";
    }
}
